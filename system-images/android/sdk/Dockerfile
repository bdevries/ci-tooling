FROM ubuntu:20.04

# inspired by rabits/qt which we use for the gcc toolkit
MAINTAINER Aleix Pol <aleixpol@kde.org>

ARG QT_VERSION=5.15.3
ARG QT_TAG=kde/5.15
ARG NDK_VERSION=r22b
ARG SDK_PLATFORM=android-30
ARG SDK_BUILD_TOOLS=30.0.2
ARG MIN_NDK_PLATFORM=android-21
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV QT_PATH /opt/Qt
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-ndk
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${MIN_NDK_PLATFORM}
ENV ANDROID_API_VERSION ${SDK_PLATFORM}
ENV ANDROID_BUILD_TOOLS_REVISION ${SDK_BUILD_TOOLS}
ENV PATH ${QT_PATH}/bin:${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}

# Install updates & requirements:
#  * unzip - unpack platform tools
#  * git, openssh-client, ca-certificates - clone & build
#  * locales, sudo - useful to set utf-8 locale & sudo usage
#  * curl - to download Qt bundle
#  * make, openjdk-8-jdk, ant - basic build requirements
#  * libsm6, libice6, libxext6, libxrender1, libfontconfig1, libdbus-1-3 - dependencies of Qt bundle run-file
#  * libc6:i386, libncurses5:i386, libstdc++6:i386, libz1:i386 - dependencides of android sdk binaries
RUN dpkg --add-architecture i386 && apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    unzip \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    make \
    openjdk-8-jdk \
    ant \
    build-essential \
    python3 \
    libsm6 \
    libice6 \
    libxext6 \
    libxrender1 \
    libfontconfig1 \
    libdbus-1-3 \
    libc6:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    libz1:i386 \
    #build dependencies
    rdfind \
    python3-distutils \
    python3-future \
    python3-xdg \
    python3-requests \
    libxml-simple-perl \
    libjson-perl \
    libio-socket-ssl-perl \
    libyaml-perl libyaml-libyaml-perl \
    ninja-build \
    build-essential \
    gperf gettext \
    python3 python3-paramiko python3-lxml python3-yaml \
    bison flex \
    ruby wget \
    #for dbus/expat
    automake libtool autoconf autoconf-archive pkg-config \
    #for translations
    subversion gnupg2 \
    #for karchive
    zlib1g-dev \
    # for craft
    p7zip-full \
    python3-pip \
    python3-venv \
    && apt-get -qq clean \
    && locale-gen en_US.UTF-8 && dpkg-reconfigure locales \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10

# Install components needed by Gitlab CI
RUN pip install python-gitlab packaging

##########################

RUN chmod a+w /opt/

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

RUN mkdir /output
RUN chown user:user /output

USER user
WORKDIR /home/user
ENV HOME /home/user

##########################

# Download & unpack android SDK
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip' \
    && mkdir -p /opt/android-sdk/cmdline-tools && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk/cmdline-tools && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" ${SDK_PACKAGES} && sdkmanager --uninstall --verbose emulator

# Download & unpack android NDK
RUN mkdir /tmp/android && cd /tmp/android && curl -Lo ndk.zip "https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip" \
    && unzip -q ndk.zip && mv android-ndk-* $ANDROID_NDK_ROOT && chmod -R +rX $ANDROID_NDK_ROOT \
    && rm -rf /tmp/android \
    && rdfind -makehardlinks true -makeresultsfile false /opt/android-ndk

COPY build-openssl /opt/helpers/
RUN bash /opt/helpers/build-openssl

RUN export OPENSSL_LIBS='-L/opt/kdeandroid-arm/lib -lssl -lcrypto' && \
    cd && git clone https://invent.kde.org/qt/qt/qt5.git --single-branch --branch ${QT_TAG} && \
    cd qt5 && \
    ./init-repository && \
    ./configure -xplatform android-clang -openssl-runtime -nomake tests -nomake examples -android-abis armeabi-v7a,arm64-v8a -skip qtquick3d -skip qttranslations -skip qtserialport -skip qtwebengine -no-warnings-are-errors -opensource -confirm-license -prefix $QT_PATH -I/opt/kdeandroid-arm/include && \
    make -j`nproc` && \
    make -j`nproc` install && \
    cd .. && rm -rf qt5
##########################

# Update build tools template
RUN sed -i.bak "s/buildToolsVersion '.*'/buildToolsVersion androidBuildToolsVersion/" /opt/Qt/src/android/templates/build.gradle

RUN mkdir /opt/nativetooling

ENV ANDROID_NDK $ANDROID_NDK_ROOT
COPY gitconfig $HOME/.gitconfig

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://cmake.org/files/v3.19/cmake-3.19.1-Linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

# makes sure gradle is downloaded, otherwise it will be downloaded every time
RUN $QT_PATH/src/3rdparty/gradle/gradlew

# compile kf5 tooling
RUN cd && git clone https://invent.kde.org/qt/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase \
    && ./configure -prefix /opt/nativetooling -opensource -confirm-license -no-gui -release -optimize-size -nomake tests -nomake examples -no-feature-concurrent \
    && make -j`nproc` && make install && rm -rf ~/qtbase


# QtQml Needed for native tooling (ki18n)
RUN cd && git clone https://invent.kde.org/qt/qt/qtdeclarative.git --single-branch --branch ${QT_TAG} && cd qtdeclarative \
    && QMAKESPEC=linux-g++ /opt/nativetooling/bin/qmake . && make -j`nproc` && make install && rm -rf ~/qtdeclarative

COPY build-cmake-native git-clone-macro.sh /opt/helpers/
ENV PERSIST=0
RUN /opt/helpers/build-cmake-native extra-cmake-modules kde:extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH="$QT_PATH;/opt/nativetooling" -DANDROID_ABI=arm64-v8a
RUN /opt/helpers/build-cmake-native kconfig     kde:kconfig     -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DKCONFIG_USE_GUI=OFF
RUN /opt/helpers/build-cmake-native kcoreaddons kde:kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=ON

COPY build-autotools build-cmake build-qt build-generic build-eigen build-poppler build-vlcqt build-gstreamer-binaries build-kde-dependencies build-kde-project create-apk get-apk-args.py \
     skip build-standalone build-discount 0001-Hack-Use-mktags-from-native-build-when-crosscompilin.patch craft-bootstrap craft-entrypoint.sh build-qtkeychain 0001-Don-t-find-QtDBus-on-Android.patch \
     0001-Make-QtAndroidExtras-dependency-private.patch /opt/helpers/

ENV STANDALONE_EXTRA="--stl=libc++"

#build expat, needed for exiv2
RUN EXTRA_CMAKE_SUBDIR=expat /opt/helpers/build-cmake libexpat https://github.com/libexpat/libexpat.git

#build libical, needed for kcalendarcore
RUN GIT_EXTRA="--branch v3.0.10" /opt/helpers/build-cmake libical https://github.com/libical/libical -DICAL_GLIB=False -DLIBICAL_BUILD_TESTING=False

#build qrencode, needed fort prison
RUN GIT_EXTRA="--branch v4.1.1" /opt/helpers/build-cmake libqrencode https://github.com/fukuchi/libqrencode.git -DWITH_TOOLS=OFF

#build taglib, required for kasts
RUN GIT_EXTRA="--branch v1.12" /opt/helpers/build-cmake taglib https://github.com/taglib/taglib

#build zxing-cpp, needed for kitinerary, qrca
RUN GIT_EXTRA="--branch v1.2.0" /opt/helpers/build-cmake zxing-cpp https://github.com/nu-book/zxing-cpp.git -DBUILD_UNIT_TESTS=OFF -DBUILD_BLACKBOX_TESTS=OFF -DBUILD_EXAMPLES=OFF

# needed for Neochat
RUN GIT_EXTRA="--branch 0.30.2" /opt/helpers/build-cmake cmark https://github.com/commonmark/cmark.git

RUN GIT_EXTRA='--branch=0.6.11' /opt/helpers/build-cmake libquotient https://github.com/quotient-im/libquotient.git

RUN /opt/helpers/build-qtkeychain

RUN GIT_EXTRA='--branch=v0.2.0' /opt/helpers/build-cmake qcoro https://github.com/danvratil/qcoro -DQCORO_WITH_QTDBUS=OFF -DBUILD_TESTING=OFF

# needed for Koko
RUN GIT_EXTRA="--branch=v0.27.4" /opt/helpers/build-cmake exiv2 https://github.com/exiv2/exiv2 -DCMAKE_POSITION_INDEPENDENT_CODE=On -DEXIV2_BUILD_EXIV2_COMMAND=Off -DEXIV2_BUILD_SAMPLES=Off

# Needed for Kaidan
RUN GIT_EXTRA="--branch=v1.4.0" /opt/helpers/build-cmake qxmpp https://github.com/qxmpp-project/qxmpp.git

# Needed for Itinerary, Okular
RUN /opt/helpers/build-poppler

# Needed for itinerary
RUN GIT_EXTRA="--branch=v2.9.12" AUTOTOOLS_EXTRA='--disable-static --without-lzma --without-python --without-debug --without-docbook --without-http' /opt/helpers/build-autotools libxml2 https://gitlab.gnome.org/GNOME/libxml2.git

# needs to be after building qt, otherwise it breaks weirdly
ENV QMAKESPEC android-clang

RUN /opt/helpers/build-cmake libintl-lite https://github.com/j-jorge/libintl-lite.git
ENV PATH ${PATH}:/opt/helpers

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV PERSIST=1

# if Craft has been set up in a persistent volume, load the Craft environment for
# both externally supplied commands as well as the default shell
ENTRYPOINT [ "/opt/helpers/craft-entrypoint.sh" ]
CMD [ "/bin/bash" ]
